#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>

int main(void) {
	CvCapture * capture = cvCreateCameraCapture(CV_CAP_ANY);
	if(!capture) {
		fprintf(stderr, "Couldn't grab a frame\r\n");
		exit(-1);
	}
	IplImage *frame = cvQueryFrame(capture);
	if(!frame) {
		fprintf(stderr, "Frame is null\r\n");
		exit(-2);
	}
	const char filename[] = "img_cam.jpg";
	cvSaveImage(filename, frame, 0);
	cvReleaseCapture(&capture);
	printf("Image saved to %s\r\n", filename);
	return 0;
}
