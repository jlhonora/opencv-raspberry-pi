Using OpenCV in the Raspberry Pi
================================

Here's a simple example to use OpenCV in the Raspberry pi. It captures an image fron
a camera and saves it to a file.

To compile use:

	$ gcc -g `pkg-config --cflags opencv` `pkg-config --libs opencv` capture_image_from_cam.c
	$ sudo ./a.out

Make sure to have opencv installed, this worked for me:

	sudo apt-get install opencv

Then I added /usr/include to my PATH variable in ~/.bashrc
